snaps = @shell.hbase_admin.list_snapshot
snaps = snaps.select{|snap| Time.now - (60*60*24*7) > Time.at(snap.getCreationTime/1000)}
snaps.each{|snap|@shell.hbase_admin.delete_snapshot(snap.name)}
exit