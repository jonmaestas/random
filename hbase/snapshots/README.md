# Hbase Snapshot Scripts

## Snapshot All HBase Tables
/bin/hbase shell --noninteractive [snapshot_all.rb](snapshot_all.rb)

## Cleanup Snapshots older than 7 days
/bin/hbase shell --noninteractive [snapshot_cleanup.rb](snapshot_cleanup.rb)